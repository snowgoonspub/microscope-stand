# Microscope Stand
A 3D-printable adjustable microscope stand for USB microscopes.  These scopes 
are super cheap and seem to be sold under many brand names, but the OEM
appears to be [ToupTek]; An example for sale is here
[on Amazon](https://www.amazon.com/Jiusion-Magnification-Endoscope-Microscope-Compatible/dp/B06WD843ZM/).

The scopes themselves do the job pretty well (you get what you pay for, but
for tasks like surface-mount soldering, they're perfectly adequate).  But the
stand that comes with it is pretty flimsy and not good for much.  So
this project will give you a more reliable stand with a built-in focus adjustment.


[ToupTek]: http://touptek.com/product/product.php?lang=en&class2=85

### Author
Copyright (c) 2020 by Tim Walls - https://snowgoons.ro/

Licensed under a CC BY-BC-SA license - see LICENSE.md for details.

## What's in the box
| File       | Description |
| ---------- | ----------- |
| `Micro-Base-Transmission-Plate.stl` | This is the base of the holder; the gear assembly for the focus control is mounted on here |
| `Micro-Cover-Base.stl` | The cover that goes over the completed base assembly |
| `Micro-Gear-Thumbwheel.stl` | The thumbwheel gear for focus control |
| `Micro-Gear-Main.stl` | The main gear that transmits thumbwheel movement to the two wormscrews |
| `Micro-Worm-Drive.stl` | The wormscrews that the scope holder is supported by.  You will need to print *two* of these |
| `Micro-Scope-Holder.stl` | The holder for the scope itself, this is driven up and down the wormscrews when the focus thumbwheel is turned |
| `Micro-Cover-Cap.stl` | The cap that sits on top of the guide pillars from the base, to finish everythign off |

Refer to the images [`Renders/Final Assembly - Gears.png`](Renders/Final Assembly - Gears.png) 
and [`Renders/Final Assembly.png`](Renders/Final Assembly.png) to see how things are put together.

## Printing and Assembly Notes
You will want to have a small file or some sandpaper on hand to finish off the edges of thoe holes
in the base cover that go over the support pillars.

The base cover attaches to the base with four M3 screws.  You'll likely want to
use a thread tapper to actually tap the threads in these.  Or, you could just
use self-tapping woodscrews.

The top cap can be glued into place once everything else is together.

The channel along the scope holder component is there to use as a cable guide
for the cable from the microscope, to keep things neat.

### Special Note - Gears
The gears in particular are designed to fairly tight tolerances - we don't want
them sloppy, and if the two wormscrews aren't synced up there's a risk that the
cope holder could jam.  That means as they come out of your printer there is a
risk they don't *quite* fit together and move freely with the main gear.

Literally one or two strokes on each of the teeth of the wormdrives with a small
file (or rough sandpaper) should be all you need to get everything working
smoothly.

### Special Note - Fitting the Scope
You will want to slide the scope into its mounting *lengthways* (i.e. feed the
scope into the large mounting hole until it snaps into place.)  If you try and
insert it from the side, you *will* break the mounting clip (unless you are
printing with ABS or something, but even then it's not designed for that.)

## Printing Tips
THere are no tricky overhangs or anything; but since we have gears and holes
with tight tolerances, you'll want to use whatever settings give you the
best dimensional accuracy.  I used:

* Layer height: 0.2mm
* Nozzle width: 0.4mm
* Infill: Spherical, 15%
* Supports: No
* Rafts/brim: No
* Material: Colorfabb PLA/PHA

